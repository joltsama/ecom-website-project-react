import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { fetchProduct } from '../redux/actionCreators/products'
import Rating from 'react-rating'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { addFavourite } from '../redux/actionCreators/favourite'
import { deleteFavourite } from '../redux/actionCreators/favourite'
import { addCart } from '../redux/actionCreators/cart'
import { faHeart, faStar } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'

const mapStateToProps = state => ({
    products: state.products,
    favourite: state.favourite
})

const mapDispatchToProps = dispatch => ({
    fetchProduct: (productId) => dispatch(fetchProduct(productId)),
    addFavourite: (productId) => dispatch(addFavourite(productId)),
    deleteFavourite: (productId) => dispatch(deleteFavourite(productId)),
    addToCart: (productId) => dispatch(addCart(productId))
})

export class ProductDetail extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const productId = this.props.match.params.productId;
        const data = this.props.products.products.filter(item => item._id === productId)[0];
        if (!data) {
            this.props.fetchProduct(productId);
        }
    }

    toggleFav(isFavourite, productId) {
        console.log('t')
        if (isFavourite)
            this.props.deleteFavourite(productId)
        else
            this.props.addFavourite(productId)
    }

    addProductToCart(productId) {
        this.props.addToCart(productId)
    }

    render() {
        const productId = this.props.match.params.productId;
        const isFavourite = this.props.favourite.favourite.some(el => el === productId);
        const data = this.props.products.products.filter(item => item._id === productId)[0];
        if (this.props.products.errMess) {
            return (
                <section className="section is-medium has-text-centered">
                    {this.props.products.errMess}
                </section>
            )
        }
        if (data) {
            return (
                <section className="section">
                    <div className="container">
                        <div className="columns">
                            <div className="column">
                                <figure>
                                    <img src={data.imgsrc} height={500} width={500} alt="product" />
                                </figure>
                            </div>
                            <div className="column is-vcentered">
                                <div className="level">
                                    <div className="level-left">
                                        <div className="level-item">
                                        </div>
                                    </div>
                                    <div className="level-right">
                                        <div className="level-item">
                                            <span className="icon is-right">
                                                <FontAwesomeIcon size="2x" icon={isFavourite ? faHeart : farHeart}
                                                    onClick={(e) => {
                                                        e.preventDefault();
                                                        this.toggleFav(isFavourite, productId);
                                                    }}
                                                />
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <h1 className="title is-4 mb-1">{data.name}</h1>
                                <Rating
                                    emptySymbol={<FontAwesomeIcon icon={farStar} />}
                                    fullSymbol={<FontAwesomeIcon icon={faStar} />}
                                    // fullSymbol={<FontAwesomeIcon icon={faStar} />}
                                    placeholderSymbol={<FontAwesomeIcon icon={faStar} />}
                                    className="rating mb-2" start="0" stop="5" step="1" fractions="2" placeholderRating="3.5"
                                />
                                <p className="title is-5 ">${data.price || 0}</p>
                                <p className="subtitle ">{data.desc}</p>
                                <p className="mb-2">{data.extdesc}</p>
                                <div className="buttons">
                                    <div className="button is-medium">
                                        Buy Now!
                                    </div>
                                    <div className="button is-medium" onClick={() => this.addProductToCart(productId)}>
                                        Add to Cart
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            )
        } else {
            return (
                <section className="section is-medium has-text-centered">Loading ...</section>
            )
        }


    }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProductDetail))
