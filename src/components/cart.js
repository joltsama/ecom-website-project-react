import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux'
import { fetchCart, deleteCart } from '../redux/actionCreators/cart'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCartPlus } from '@fortawesome/free-solid-svg-icons'

import '../styles/cart.scss'

const mapStateToProps = (state) => ({
    cart: state.cart
})

const mapDispatchToProps = (dispatch) => ({
    deleteFromCart: (productId) => dispatch(deleteCart(productId)),
    fetchCart: () => dispatch(fetchCart())
})

function CartItem(props) {
    return (
        <article className="media">
            <figure className="media-left">
                <p className="image is-128x128">
                    <img className="inherit-img-dim" src={props.item.imgsrc} alt="product" />
                </p>
            </figure>
            <div className="media-content">
                <div className="columns">
                    <div className="column is-9">
                        <h1 className="is-size-4">{props.item.name}</h1>
                        <div className="content">
                            <p>
                                {props.item.desc}
                            </p>
                        </div>
                    </div>
                    <div className="column is-3">
                        <p className="is-size-4">
                            ${props.item.price || 0}
                        </p>
                    </div>
                </div>
            </div>
            <div className="media-right">
                <button className="delete" onClick={() => props.delete()}></button>
            </div>
        </article>
    )
}

function ProductList(props) {

    useEffect(() => {
        props.fetchCart();
    }, [props.cart.cart.length])

    let CartProducts = null;
    if (props.cart.cart) {
        CartProducts = props.cart.cart.map((item) => {
            return (
                <CartItem
                    item={item}
                    key={item._id}
                    delete={() => { props.deleteFromCart(item._id) }}
                />
            )
        })
    }
    if (CartProducts.length)
        return (
            <div>

                {CartProducts}
            </div>
        )
    else {
        return (
            <section className="section is-medium has-text-centered">

                <p className="is-size-5">
                    <span class="icon">
                        <FontAwesomeIcon icon={faCartPlus} />
                    </span>
                    Cart Empty.
                </p>
                <p className="is-size-5">Please add some items to cart</p>
            </section>
        )
    }
}
const ConnectedProductList = connect(mapStateToProps, mapDispatchToProps)(ProductList)

function Cart() {
    return (
        <section className="hero">
            <div className="hero-body">
                <div className="container">
                    <h1 className="title is-4">
                        Cart
                    </h1>
                    <div className="subtitle">
                        Products that interest you!
                    </div>
                    <hr className="hr" />
                    <ConnectedProductList />
                </div>
            </div>
        </section>
    )
}

export default Cart
