import React, { useEffect } from 'react';
import { connect } from 'react-redux'
import { fetchFavourite, deleteFavourite } from '../redux/actionCreators/favourite'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import '../styles/favourite.scss'

const mapStateToProps = (state) => ({
    favourite: state.favourite
})

const mapDispatchToProps = (dispatch) => ({
    deleteFromFavourite: (productId) => dispatch(deleteFavourite(productId)),
    fetchFavourite: () => dispatch(fetchFavourite())
})

function FavItem(props) {
    return (
        <article className="media">
            <figure className="media-left">
                <p className="image is-128x128">
                    <img className="inherit-img-dim" src={props.item.imgsrc} alt="Product" />
                </p>
            </figure>
            <div className="media-content">
                <div className="columns">
                    <div className="column is-9">
                        <h1 className="is-size-4">{props.item.name}</h1>
                        <div className="content">
                            <p>
                                {props.item.desc}
                            </p>
                        </div>
                    </div>
                    <div className="column is-3">
                        <p className="is-size-4">
                            {props.item.price || 0}
                        </p>
                        <p className={"is-size-4" + (props.item.price ? '' : " is-hidden")}>
                            <a href='#' onClick={(e) => { e.preventDefault(); }}>Add to Cart?</a>
                        </p>
                    </div>
                </div>
            </div>
            <div className="media-right">
                <button className="delete" onClick={() => props.delete()}></button>
            </div>
        </article>
    )
}

function ProductList(props) {
    useEffect(() => {
        props.fetchFavourite();
    }, [props.favourite.favourite.length])

    let FavouriteProducts = null;
    if (props.favourite.favourite) {
        FavouriteProducts = props.favourite.favourite.map((item) => (
            <FavItem
                item={item}
                key={item._id}
                delete={() => { props.deleteFromFavourite(item._id) }}
            />
        ))
    }
    if (FavouriteProducts.length)
        return (
            <div>
                {FavouriteProducts}
            </div>
        )
    else {
        return (
            <section className="section is-medium has-text-centered">
                <p className="is-size-5">
                    <span className="icon">
                        <FontAwesomeIcon className="fas" icon={faHeart} />
                    </span>
                    No favourites
                 </p>

                <p className="is-size-5">It seems you dont have any favourites yet.</p>
                <p className="is-size-5">Add some products to favourites to see them here</p>
            </section>
        )
    }
}
const ConnectedProductList = connect(mapStateToProps, mapDispatchToProps)(ProductList)

function Favourite() {
    return (
        <section className="hero">
            <div className="hero-body">
                <div className="container">
                    <h1 className="title is-4">
                        Favourites
                    </h1>
                    <div className="subtitle">
                        A place to keep all your interests
                    </div>
                    <hr className="hr" />
                    <ConnectedProductList />
                </div>
            </div>
        </section>
    )
}

export default Favourite
