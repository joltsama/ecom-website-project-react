import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { register, registerClear } from '../redux/actionCreators/register'

const mapStateToProps = (state) => ({
    auth: state.auth,
    registeration: state.register
})

const mapDispatchToProps = (dispatch) => ({
    register: (user) => dispatch(register(user)),
    registerClear: (user) => dispatch(registerClear(user))
})

export class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            user: {
                firstName: '',
                lastName: '',
                username: '',
                email: '',
                password: '',
                seller: false,
            },
            userError: '',
            passError: '',
            emailError: '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        if (this.props.registeration.registerd) {
            this.props.registerClear();
        }
    }

    validateForm() {
        var valid = true;
        if (this.state.user.username.includes('!')) {
            this.setState({ userError: 'Username cant include - !' })
            valid = false;
        }
        if (this.state.user.password.includes('@')) {
            this.setState({ passError: 'Not a valid password.' })
            valid = false;
        }
        if (!this.state.user.email.includes('@')) {
            this.setState({ emailError: 'Not a valid email.' })
            valid = false;
        }
        return valid;
    }

    handleSubmit(e) {
        e.preventDefault();
        if (this.validateForm()) {
            this.props.register(this.state.user);
        }
    }

    handleChange(e) {
        e.preventDefault();
        const { name, value, checked } = e.target;
        const { user } = this.state;
        let val = value;
        if (name === 'seller') {
            val = checked;
        }
        this.setState({
            user: { ...user, [name]: val }
        });
    }

    render() {
        if (this.props.auth.loggedIn && this.props.auth.user)
            return (<Redirect to="/" />)

        const { registering, registerd, errMess } = this.props.registeration;

        return (
            <section className="hero">
                <div className="hero-body">
                    <div className="container">
                        <div className="columns">
                            <div className="column is-3">
                                <div className="title is-4  mb-7">
                                    Register
                                </div>
                                <p className={"message is-link" + (registerd ? "" : " is-hidden")}>Registration successfull! Please login</p>
                                <p className="message is-danger">{errMess}</p>
                                <form className="form" name="form" onSubmit={this.handleSubmit}>
                                    <div className="field">
                                        <label class="label">Username</label>
                                        <div className="control">
                                            <input className="input"
                                                value={this.state.user.username}
                                                onChange={(e) => { this.handleChange(e) }}
                                                name="username"
                                                type="text" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">First Name</label>
                                        <div className="control">
                                            <input className="input"
                                                value={this.state.user.firstName}
                                                onChange={(e) => { this.handleChange(e) }}
                                                name="firstName"
                                                type="text" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">Last Name</label>
                                        <div className="control">
                                            <input className="input"
                                                value={this.state.user.lastName}
                                                onChange={(e) => { this.handleChange(e) }}
                                                name="lastName"
                                                type="text" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">Email</label>
                                        <div className="control">
                                            <input className="input"
                                                value={this.state.user.email}
                                                onChange={(e) => { this.handleChange(e) }}
                                                name="email"
                                                type="email" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">Password</label>
                                        <div className="control">
                                            <input
                                                className="input"
                                                value={this.state.user.password}
                                                onChange={(e) => { this.handleChange(e) }}
                                                name="password"
                                                type="password" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">Are you a seller?</label>
                                        <div className="control">
                                            <label className="checkbox">
                                                <input
                                                    checked={this.state.user.seller}
                                                    onChange={(e) => { this.handleChange(e) }}
                                                    name="seller"
                                                    type="checkbox"
                                                />
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div className="field">

                                        <div className="control">
                                            <button className={"button" + (registering ? " is-loading" : "")} onClick={(e) => { this.handleSubmit(e) }}>
                                                Register
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Register)