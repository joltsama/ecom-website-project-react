import React, { Component } from 'react'
import { useHistory } from "react-router-dom";
import { NavLink, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { logout } from '../redux/actionCreators/auth'
import '../styles/navbar.scss'

const mapStateToProps = (state) => ({
  auth: state.auth
})

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logout())
})

function NavBarUserMenu(props) {
  let history = useHistory();
  const logout = () => {
    props.logout();
    history.push('/login')
  }
  if (props.auth.loggedIn && !props.auth.user.seller) {
    return (
      <div className="navbar-item has-dropdown is-hoverable">
        <a className="navbar-link">
          <span className="icon">
            <i className="fas fa-user"></i>
          </span>
          <b>{props.auth.user.username}</b>
        </a>

        <div className="navbar-dropdown">
          <Link to="/cart" className="navbar-item">
            Cart
            </Link>
          <Link to="/favourite" className="navbar-item">
            Favourites
            </Link>
          <hr className="navbar-divider" />
          <a className="navbar-item" onClick={() => logout()}>
            Logout
          </a>
        </div>
      </div>
    )
  }
  if (props.auth.loggedIn && props.auth.user.seller) {
    return (
      <div className="navbar-item has-dropdown is-hoverable">
        <a className="navbar-link">
          <span className="icon">
            <i className="fas fa-user"></i>
          </span>
          <b>{props.auth.user.username}</b>
        </a>

        <div className="navbar-dropdown">
          <Link to="/manageproducts" className="navbar-item">
            My products
          </Link>
          <hr className="navbar-divider" />
          <a className="navbar-item" onClick={() => logout()}>
            Logout
            </a>
        </div>
      </div>
    )
  }
  return (
    <>
      <div className="navbar-item">
        <Link to="/login" className="button is-dark is-outlined" >
          Login
      </Link>
      </div>
      <div className="navbar-item">
        <Link to="/register" className="button is-dark" >
          Register
      </Link>
      </div>
    </>
  )
}

const ConnectedNavBarUserMenu = connect(mapStateToProps, mapDispatchToProps)(NavBarUserMenu)

export class Navbar extends Component {

  constructor(props) {
    super(props);
    this.state = {
      mobileMenu: false
    }
  }

  render() {
    return (
      <nav className="navbar is-fixed-top has-shadow is-spacious">
        <div className="container">

          <div className="navbar-brand">
            <div className="navbar-item">
              <Link to="/" className="title">XYZ</Link>
            </div>
            <span className="navbar-burger burger">
              <span></span>
              <span></span>
              <span></span>
            </span>
          </div>

          <div className={"navbar-menu " + (this.state.mobileMenu === true ? "is-active" : "")}>
            <div className="navbar-start">
              <a className="navbar-item">
                <b>WHY XYZ</b>
              </a>
              <NavLink className="navbar-item" to="/products" activeClassName="is-active">
                <b>PRODUCTS</b>
              </NavLink>
              <a className="navbar-item active">
                <b>ABOUT</b>
              </a>
              <NavLink className="navbar-item" to="/contact" activeClassName="is-active">
                <b>CONTACT</b>
              </NavLink>
            </div>
            <div className="navbar-end">
              <ConnectedNavBarUserMenu />
            </div>
          </div>
        </div>
      </nav >
    )
  }
}

export default Navbar