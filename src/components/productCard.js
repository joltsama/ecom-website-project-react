import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Rating from 'react-rating'
import { addFavourite } from '../redux/actionCreators/favourite'
import { deleteFavourite } from '../redux/actionCreators/favourite'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart, faStar } from '@fortawesome/free-solid-svg-icons'
import { faHeart as farHeart } from '@fortawesome/free-regular-svg-icons'
import { faStar as farStar } from '@fortawesome/free-regular-svg-icons'

const mapStateToProps = (state) => ({
    products: state.products,
    favourite: state.favourite
})

const mapDispatchToProps = (dispatch) => ({
    addFavourite: (productId) => dispatch(addFavourite(productId)),
    deleteFavourite: (productId) => dispatch(deleteFavourite(productId))
})

export class ProductCard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isFavourite: false
        }
        this.toggleFav = this.toggleFav.bind(this)
        this.goToProduct = this.goToProduct.bind(this)
    }

    toggleFav(isFavourite, productId) {
        if (isFavourite)
            this.props.deleteFavourite(productId)
        else
            this.props.addFavourite(productId)
    }

    goToProduct(productId) {
        this.props.history.push('/products/' + productId)
    }



    render() {
        const isFavourite = this.props.favourite.favourite.some(el => el === this.props.item._id);
        return (
            <a className="card" >

                <Link to={"/products/" + this.props.item._id} className="card-image" >
                    <figure className="image is-4by3">
                        <img src={this.props.item.imgsrc} alt="product" />
                    </figure>
                </Link>

                <div className="card-content">
                    <div className="level">
                        <div className="level-left">
                            <div className="level-item">
                                <Rating
                                    emptySymbol={<FontAwesomeIcon icon={farStar} />}
                                    fullSymbol={<FontAwesomeIcon icon={faStar} />}
                                    // fullSymbol={<FontAwesomeIcon icon={faStar} />}
                                    placeholderSymbol={<FontAwesomeIcon icon={faStar} />}
                                    className="rating" start="0" stop="5" step="1" fractions="2" placeholderRating="3.5" readonly="true"
                                />
                            </div>
                        </div>
                        <div className="level-right">
                            <div className="level-item">
                                <span className="icon is-right">
                                    <FontAwesomeIcon icon={isFavourite ? faHeart : farHeart}
                                        onClick={(e) => {
                                            e.preventDefault();
                                            this.toggleFav(isFavourite, this.props.item._id);
                                        }}
                                    />
                                </span>
                            </div>
                        </div>
                    </div>

                    <div className="title is-5 mb-1">{this.props.item.name}</div>
                    <div className="content">
                        <p>{this.props.item.desc}</p>
                        <p className="title is-5">${this.props.item.price || 0}</p>
                    </div>
                </div>
            </a >
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard)