import React, { useState } from 'react'

function ManageCard(props) {
    const [menuVisible, setMenuVisible] = useState(false);
    const { data } = props;
    return (
        <div className="card"
            onMouseEnter={() => setMenuVisible(true)}
            onMouseLeave={() => setMenuVisible(false)}
        >

            <div className="card-image">

                <figure className="image is-4by3">
                    <img src={data.imgsrc} alt="product" />
                </figure>

            </div>
            {menuVisible === false ?
                <div className="card-content">
                    <div className="title is-4">{data.name}</div>
                    <div className="content">
                        <p>{data.desc}</p>
                    </div>
                </div>
                :

                <div className="card-content">
                    <div className="buttons">
                        <button className="button is-fullwidth" onClick={() => props.openViewModal(data)}>VIEW</button>
                        <button className="button is-fullwidth" onClick={() => props.openEditModal(data)}>EDIT</button>
                        <button className="button is-fullwidth" onClick={() => props.openDeleteModal(data._id)}>DELETE</button>
                    </div>
                 </div>
            }
        </div >
    )
}

export default ManageCard