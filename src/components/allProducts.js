import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchAllProducts } from '../redux/actionCreators/products'
import ProductCard from './productCard'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown, faAngleUp, faBook } from '@fortawesome/free-solid-svg-icons'
import '../styles/products.scss'

const mapStateToProps = state => ({
  products: state.products,
  favourite: state.favourite
})

const mapDispatchToProps = dispatch => ({
  fetchAllProducts: () => dispatch(fetchAllProducts())
})

function NoProducts() {
  return (
    <section className="section is-medium has-text-centered">
      <p className="is-size-5 has-text-centered">
        <span className="icon">
          <FontAwesomeIcon className="fas" icon={faBook} />
        </span>
        No products
      </p>
      <p className="is-size-5  has-text-centered">There are no products available right now. Please come back later</p>
    </section>
  )
}
export class AllProducts extends Component {

  constructor(props) {
    super(props);
    this.state = {
      sortDropDown: false,
      notification: 'None'
    }
  }

  componentDidMount() {
    this.props.fetchAllProducts();
  }

  render() {
    let Cards = "";
    if (this.props.products.products) {
      Cards = this.props.products.products.map((item) => {
        return (
          <ProductCard
            item={item}
            key={item._id}
          />
        )
      })
    }

    return (
      <>
        <section className="hero">
          <div className="hero-body">
            <div className="container">
              <h1 className="title is-4">
                All Products
              </h1>
              <div className="subtitle">
                Choose from our wide variety of products
              </div>

              <div className="field is-grouped is-grouped-right">
                <div className="control">
                  <div className="field has-addons">
                    <div className="control">
                      <input className="input" type="text" placeholder="Search for a product" />
                    </div>
                    <div className="control">
                      <a className="button">
                        Search
                  </a>
                    </div>
                  </div>
                </div>
                <div className="control">
                  <div className={"dropdown" + (this.state.sortDropDown === true ? " is-active" : "")}>
                    <div className="dropdown-trigger">
                      <button className="button" aria-haspopup="true" aria-controls="dropdown-menu3"
                        onMouseEnter={() => { this.setState({ sortDropDown: true }) }}
                        onMouseLeave={() => { this.setState({ sortDropDown: false }) }}
                      >
                        <span>Sort by</span>
                        <span className="icon is-small">

                          <FontAwesomeIcon icon={this.state.sortDropDown ?
                            faAngleUp : faAngleDown} />
                        </span>
                      </button>
                    </div>
                    <div className="dropdown-menu" id="dropdown-menu-sort"
                      onMouseEnter={() => { this.setState({ sortDropDown: true }) }}
                      onMouseLeave={() => { this.setState({ sortDropDown: false }) }}
                      role="menu">
                      <div className="dropdown-content">
                        <a href="#" className="dropdown-item">
                          Price: low to high
                          </a>
                        <a href="#" className="dropdown-item">
                          Price: high to low
                          </a>
                        <a href="#" className="dropdown-item">
                          Relevance
                          </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <hr className="hr" />
              {Cards.length ? <div className="products">{Cards}</div> : <NoProducts />}
            </div>
          </div>


        </section>

      </>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AllProducts);
