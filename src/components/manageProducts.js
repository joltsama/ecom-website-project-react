import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchAllProducts } from '../redux/actionCreators/products'
import ManageCard from './manageCard'
import { addProduct } from '../redux/actionCreators/products'
import { editProduct } from '../redux/actionCreators/products'
import { deleteProduct } from '../redux/actionCreators/products'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faPlus } from '@fortawesome/free-solid-svg-icons'
import '../styles/products.scss'

const mapStateToProps = state => ({
    products: state.products
})

const mapDispatchToProps = dispatch => ({
    fetchAllProducts: () => dispatch(fetchAllProducts()),
    addProduct: (product) => dispatch(addProduct(product)),
    editProduct: (product) => dispatch(editProduct(product)),
    deleteProduct: (productId) => dispatch(deleteProduct(productId))
})

export class ManageProducts extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeModal: '',
            productId: '',
            name: '',
            desc: '',
            price: 0,
            extdesc: '',
            imgsrc: '',
            notification: 'None'
        }
        this.openViewModal = this.openViewModal.bind(this)
        this.openEditModal = this.openEditModal.bind(this)
        this.openDeleteModal = this.openDeleteModal.bind(this)
    }

    componentDidMount() {
        if (!this.props.products.products.length)
            this.props.fetchAllProducts();
    }


    openViewModal(product) {
        this.setState({
            activeModal: "view",
            productId: product._id,
            name: product.name,
            desc: product.desc,
            extdesc: product.extdesc,
            imgsrc: product.imgsrc
        })
    }

    openEditModal(product) {
        this.setState({
            activeModal: "edit",
            productId: product._id,
            name: product.name,
            desc: product.desc,
            extdesc: product.extdesc,
            imgsrc: product.imgsrc
        })
    }

    openDeleteModal(productId) {
        this.setState({
            activeModal: "delete",
            productId: productId
        })
    }

    closeModal() {
        this.setState({
            activeModal: ''
        })
    }

    editProduct(e) {
        e.preventDefault();
        const product = {
            productId: this.state.productId,
            name: this.state.name,
            desc: this.state.desc,
            extdesc: this.state.extdesc,
            imgsrc: this.state.imgsrc,
            price: this.state.price
        }
        this.props.editProduct(product);
        this.resetData();
        this.closeModal();
    }

    addProduct(e) {
        e.preventDefault();
        const product = {
            name: this.state.name,
            desc: this.state.desc,
            extdesc: this.state.extdesc,
            imgsrc: this.state.imgsrc,
            price: this.state.price
        }
        this.props.addProduct(product);
        this.resetData();
        this.closeModal();
    }

    deleteProduct() {
        this.props.deleteProduct(this.state.productId);
        this.resetData();
        this.closeModal();
    }

    resetData() {
        this.setState({
            productId: '',
            name: '',
            desc: '',
            extdesc: '',
            imgsrc: '',
            price: 0
        })
    }

    render() {

        let Cards = "";
        if (this.props.products.products != null) {
            Cards = this.props.products.products.map((item) => {
                return (
                    <ManageCard
                        data={item}
                        key={item._id}
                        openViewModal={this.openViewModal}
                        openEditModal={this.openEditModal}
                        openDeleteModal={this.openDeleteModal}
                    />
                )
            })
        }


        return (
            <div>
                <section className="hero">
                    <div className="hero-body">
                        <div className="container">
                            <b className="is-size-4">My Products</b>
                            <div className="level">
                                <div className="level-left">
                                    <div className="level-item">
                                        Add, view and delete products.
                                    </div>
                                </div>

                                <div className="level-right">
                                    <div className="level-item">
                                        <div className="buttons is-right">
                                            <button className="button" onClick={(e) => { e.preventDefault(); this.props.fetchAllProducts(); }}>Refresh</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <p>{this.props.products.errMess}</p>
                            <hr className="hr" ></hr>

                            <div className="products">
                                {Cards}
                                <div className="card" onClick={() => { this.setState({ activeModal: "add" }) }} >
                                    <div className="card-content addicon has-text-centered">
                                        <FontAwesomeIcon icon={faPlus} size="2x" />
                                        <p className="title is-4">ADD</p>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>


                </section>
                <div className={this.state.activeModal === "add" ? "modal is-active" : "modal"} >
                    <div className="modal-background" onClick={() => { this.closeModal() }} ></div>
                    <div className="modal-content">
                        <div className="box">
                            <form className="form">

                                <div className="field">
                                    <label className="label">Name</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="Enter name"
                                            value={this.state.name}
                                            onChange={(e) => { this.setState({ name: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Description</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="Enter description"
                                            value={this.state.desc}
                                            onChange={(e) => { this.setState({ desc: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Extended Description</label>
                                    <div className="control">
                                        <textarea className="textarea" placeholder="Enter extended description"
                                            value={this.state.extdesc}
                                            onChange={(e) => { this.setState({ extdesc: e.target.value }) }}
                                        ></textarea>
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Image URL</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="URL"
                                            value={this.state.imgsrc}
                                            onChange={(e) => { this.setState({ imgsrc: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Price</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="Enter name"
                                            value={this.state.price}
                                            onChange={(e) => { this.setState({ price: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <br />
                                <div className="control is-grouped-rights">
                                    <button className="button is-link" onClick={(e) => { this.addProduct(e) }}>Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                    <button className="modal-close is-large" onClick={() => { this.closeModal() }} aria-label="close"></button>
                </div>

                <div className={this.state.activeModal === "edit" ? "modal is-active" : "modal"} >
                    <div className="modal-background" onClick={() => { this.closeModal() }} ></div>
                    <div className="modal-content">
                        <div className="box">
                            <form className="form">

                                <div className="field">
                                    <label className="label">Name</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="Enter name"
                                            value={this.state.name}
                                            onChange={(e) => { this.setState({ name: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Description</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="Enter description"
                                            value={this.state.desc}
                                            onChange={(e) => { this.setState({ desc: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Extended Description</label>
                                    <div className="control">
                                        <textarea className="textarea" placeholder="Enter extended description"
                                            value={this.state.extdesc}
                                            onChange={(e) => { this.setState({ extdesc: e.target.value }) }}
                                        ></textarea>
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Image URL</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="URL"
                                            value={this.state.imgsrc}
                                            onChange={(e) => { this.setState({ imgsrc: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <div className="field">
                                    <label className="label">Price</label>
                                    <div className="control">
                                        <input className="input" type="text" placeholder="Enter name"
                                            value={this.state.price}
                                            onChange={(e) => { this.setState({ price: e.target.value }) }}
                                        />
                                    </div>
                                </div>
                                <br />
                                <div className="control is-grouped-rights">
                                    <button className="button is-link" onClick={(e) => { this.editProduct(e) }}>Submit</button>
                                </div>
                            </form>

                        </div>
                    </div>
                    <button className="modal-close is-large" onClick={() => { this.closeModal() }} aria-label="close"></button>
                </div>

                <div className={this.state.activeModal === "delete" ? "modal is-active" : "modal"} >
                    <div className="modal-background" onClick={() => { this.closeModal() }} ></div>
                    <div className="modal-content">
                        <div className="box">
                            <p>Are you sure you want to delete this item?</p>
                            <div className="field is-grouped is-grouped-right">
                                <div className="control">
                                    <button className="button is-link" onClick={() => { this.deleteProduct() }}>Yes</button>
                                </div>
                                <div className="control">
                                    <button className="button is-link" onClick={() => { this.closeModal() }}>No</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button className="modal-close is-large" onClick={() => { this.closeModal() }} aria-label="close"></button>
                </div>

                <div className={this.state.activeModal === "view" ? "modal is-active" : "modal"} >
                    <div className="modal-background" onClick={() => { this.closeModal() }} ></div>
                    <div className="modal-card">
                        <header className="modal-card-head">
                            <p className="modal-card-title">{this.state.name}</p>
                            <button className="delete" onClick={() => { this.closeModal() }} aria-label="close"></button>
                        </header>
                        <section className="modal-card-body columns">
                            <div className="column is-half">
                                <figure className="image is-1by1">
                                    <img src={this.state.imgsrc} alt={this.state.desc} />
                                </figure>
                            </div>
                            <div className="column is-half">
                                <p>{this.state.extdesc}</p>
                            </div>
                        </section>
                    </div>
                </div>

            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageProducts);
