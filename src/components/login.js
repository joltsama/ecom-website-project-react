import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { login } from '../redux/actionCreators/auth'

const mapStateToProps = (state) => ({
    auth: state.auth
})

const mapDispatchToProps = (dispatch) => ({
    login: (username, password, userType) => dispatch(login(username, password, userType))
})

export class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            seller: false,
            err: null
        }
        this.handleChange = this.handleChange.bind(this);
        this.requestLogin = this.requestLogin.bind(this);
    }

    requestLogin(e) {
        e.preventDefault();
        console.log(this.state.username, this.state.password, this.state.seller)
        this.props.login(this.state.username, this.state.password, this.state.seller);
    }

    handleChange(e) {
        e.preventDefault();
        const { name, value, checked } = e.target;
        // console.log(name, value, checked)
        // console.log(e.target)
        let val = value;
        if (name === 'seller') {
            val = checked
        }
        this.setState({ [name]: val })
    }

    render() {
        const { loggingIn, loggedIn, errMess } = this.props.auth;

        if (loggedIn && this.props.auth.user)
            return (<Redirect to="/" />)

        return (
            <section className="hero is-medium is-secondary">
                <div className="hero-body">
                    <div className="container">
                        <div className="columns">
                            <div className="column is-3">
                                <div className="title is-4  mb-7">
                                    Login
                                </div>
                                <form>
                                    <p className="message is-danger">{errMess}</p>
                                    <div className="field">
                                        <label class="label">Username</label>
                                        <div className="control">
                                            <input className="input"
                                                value={this.state.username}
                                                onChange={this.handleChange}
                                                name="username"
                                                type="username" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">Password</label>
                                        <div className="control">
                                            <input
                                                className="input"
                                                value={this.state.password}
                                                onChange={this.handleChange}
                                                name="password"
                                                type="password" />
                                        </div>
                                    </div>
                                    <div className="field">
                                        <label class="label">Are you a seller?</label>
                                        <div className="control">
                                            <label className="checkbox">
                                                <input
                                                    // checked={this.state.seller}
                                                    onChange={this.handleChange}
                                                    name="seller"
                                                    type="checkbox"
                                                />
                                                Yes
                                            </label>
                                        </div>
                                    </div>
                                    <div className="field">
                                        <div className="control">
                                            <button className={"button" + (loggingIn ? " is-loading" : "")} onClick={this.requestLogin}>
                                                Login
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login)

