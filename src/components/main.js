import React, { Component } from 'react'
import { connect } from 'react-redux'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Navbar from './navbar'
import Front from './frontPage'
import AllProducts from './allProducts'
import ManageProducts from './manageProducts'
import Login from './login'
import Register from './register'
import Footer from './footer'
import Favourite from './favourite'
import Cart from './cart'
import { autoLogin } from '../redux/actionCreators/auth'
import ProductDetail from './productDetail'
import { PrivateRoute } from './privateRoute'
import '../styles/main.scss'
import 'bulma'
import 'bulma-divider'

const mapDispatchToProps = (dispatch) => ({
  autoLogin: () =>  dispatch(autoLogin()) 
})

export class App extends Component {

  componentDidMount() {
    this.props.autoLogin();
  }

  render() {
    return (
      <div className="main">
        <Router>
          <Navbar />
          <Switch>
            <Route exact path="/"><Front /></Route>
            <Route exact path="/products/:productId"><ProductDetail /></Route>
            <Route exact path="/products"><AllProducts /></Route>
            <Route exact path="/login"><Login /></Route>
            <Route exact path="/register"><Register /></Route>
            <PrivateRoute exact path="/favourite" component={Favourite} />
            <PrivateRoute exact path="/cart" component={Cart} />
            <PrivateRoute exact path="/manageproducts" component={ManageProducts} />
          </Switch>
        </Router>
        <Footer />
      </div>
    )
  }
}

export default connect(null, mapDispatchToProps)(App);


