import React, { Component } from 'react'
import '../styles/footer.scss'

export class Footer extends Component {
  render() {
    return (
      <footer className="footer">
        <div className="custom-container">
            <div className="columns">
              <div className="column">
                <p className="title is-4">Links </p>
                <p className="footer-link"><a>Link 1</a></p>
                <p className="footer-link"><a>Link 2</a></p>
                <p className="footer-link"><a>Link 3</a></p>
                <p className="footer-link"><a>Link 4</a></p>
                <p className="footer-link"><a>Link 5</a></p>
              </div>
              <div className="column">
                <p className="title is-4">More Links </p>
                <p className="footer-link"><a>Link 1</a></p>
                <p className="footer-link"><a>Link 2</a></p>
                <p className="footer-link"><a>Link 3</a></p>
                <p className="footer-link"><a>Link 4</a></p>
                <p className="footer-link"><a>Link 5</a></p>
              </div>
              <div className="column right-align">
                <p className="title is-4">Contact Us</p>
                <p>Street 1</p>
                <p>Some City</p>
                <p>State Name</p>
                <p>Country</p>
                <p>321423</p>
              </div>
            </div>
        </div>
      </footer>
    )
  }
}

export default Footer
