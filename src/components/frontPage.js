import React, { Component } from 'react'
import '../styles/frontPage.scss'

export class Front extends Component {
  render() {
    return (
      <section className="section first">
        <div className="custom-container">
          <div className="title">
            Front-end development in ReactJS
          </div>
          <div className="subtitle">
            An overview of ReactJS components and state
          </div>

          <figure className="image">
            <img id="frontimage" src={require("../images/front.jpg")} alt="front banner"></img>
          </figure>
          <div className="is-size-4 mt-6">
            Heading 1
            </div>
          <p className="mt-3">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
          </p>
          <hr className="hr" />
          <div className="is-size-4 mt-5">
            Heading 2
          </div>
          <p className="mt-3">
            At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.
          </p>
        </div>
      </section>
    )
  }
}

export default Front
