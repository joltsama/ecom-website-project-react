import * as ActionTypes from '../actionTypes/cart'
import axios from 'axios'
import { apiURL } from '../../config'
import {authHeader} from '../services'
// const authHeader = {
//     authorization: `Bearer ${localStorage.getItem("usertoken")}`
// }

export const addCart = (productId) => (dispatch) => { 
    axios({
        method: 'post',
        url: apiURL + '/api/cart',
        data: {
            _id: productId
        },
        headers: authHeader
    })
        .then(response => {
            if (response.status === 200)
                return dispatch(addCartSuccess(productId));
            else
                return dispatch(addCartFailed(response.status.toString()));
        })
        .catch(err => {
            dispatch(addCartFailed(err.message));
        });
}

export const addCartSuccess = (productId) => ({
    type: ActionTypes.ADD_CART_SUCCESS,
    payload: productId
})

export const addCartFailed = (errMessage) => ({
    type: ActionTypes.ADD_CART_FAILED,
    payload: errMessage
})


// FETCH
export const fetchCart = () => (dispatch) => {
    // dispatch(cartLoading());
    axios({
        method: 'get',
        url: apiURL + '/api/cart',
        headers: authHeader
    })
        .then(response => {
            if (response.status === 200) {
                return dispatch(fetchCartSuccess(response.data))
            } else {
                return dispatch(fetchCartFailed(response.status))
            }
        })
        .catch(err => {
            dispatch(fetchCartFailed(err.message))
        })
}

export const fetchCartSuccess = (data) => ({
    type: ActionTypes.FETCH_CART_SUCCESS,
    payload: data
})
export const fetchCartFailed = (errMessage) => ({
    type: ActionTypes.FETCH_CART_FAILED,
    payload: errMessage
})


// DELETE
export const deleteCart = (productId) => (dispatch) => {
    axios({
        method: 'delete',
        url: apiURL + '/api/cart',
        data: {
            _id: productId
        },
        headers: authHeader
    })
        .then(response => {
            if (response.status === 200) {
                return dispatch(deleteCartSuccess(productId));
            } else {
                return dispatch(deleteCartFailed(response.status.toString()));
            }
        })
        .catch(err => dispatch(deleteCartFailed(err.message)));
}

export const deleteCartSuccess = (productId) => ({
    type: ActionTypes.DELETE_CART_SUCCESS,
    payload: productId
})

export const deleteCartFailed = (errMessage) => ({
    type: ActionTypes.DELETE_CART_FAILED,
    payload: errMessage
})

