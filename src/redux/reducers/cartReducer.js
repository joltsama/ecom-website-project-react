
import * as ActionTypes from '../actionTypes/cart'

export const cartReducer = (
    state = {
        errMess: null,
        cart: []
    },
    action
) => {
    switch (action.type) {
        case ActionTypes.FETCH_CART_SUCCESS:
            return { errMess: null, cart: action.payload }
        case ActionTypes.FETCH_CART_FAILED:
            return { ...state, errMess: action.payload }

        case ActionTypes.ADD_CART_SUCCESS:
            if (state.cart.some(el => el === action.payload))
                return state
            return { ...state, errMess: null, cart: [...state.cart, action.payload] }

        case ActionTypes.ADD_CART_FAILED:
            return { ...state, errMess: action.payload }

        case ActionTypes.DELETE_CART_SUCCESS:
            return { errMess: null, cart: state.cart.filter((item) => item._id !== action.payload) };

        case ActionTypes.DELETE_CART_FAILED:
            return { ...state, errMess: action.payload }

        default:
            return state;
    }
}