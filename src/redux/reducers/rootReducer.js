import { combineReducers } from 'redux'
import { productsReducer } from './productsReducer'
import { cartReducer } from './cartReducer'
import { favouriteReducer } from './favouriteReducer'
import { authReducer } from './authReducer'
import { registerReducer } from './registerReducer'

export const rootReducer = combineReducers({
    products: productsReducer,
    cart: cartReducer,
    favourite: favouriteReducer,
    auth: authReducer,
    register: registerReducer
});

